public class Student {

	private int age;
	private String sid;
	
	public Student(int age, String sid) {
		this.age = age;
		this.sid = sid;
	}
	
	public String getSid() {
		return sid;
	}
	
	public void setSid(String id) {
		sid = id;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int newAge) {
		age = newAge;
	}
	
	public String toString() {
		return "Student id: " + sid + ", age: " + age;
	}
	
	public boolean equals(Student s) {
		return sid.equals(s.getSid());
	}
	
	public Student findYoungest(Student s) {
		if(age <= s.getAge())
			return this;
		return s;
	}
	

}
