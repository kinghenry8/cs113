import java.util.*;

public class Triangle 
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		double side1, side2,hypotSquare,hypot;
		
		System.out.print("Please enter side 1 of right angle triangle:");
		side1=scan.nextDouble();
		System.out.print("Please enter side 2 of right angle triangle:");
		side2=scan.nextDouble();
		
		hypotSquare = Math.pow(side1,2)+Math.pow(side2, 2);
		
		hypot = Math.sqrt(hypotSquare);
		
		System.out.println("The hypotenuse is: " + hypot);
		
		
		scan.close();
		
		
	}
}