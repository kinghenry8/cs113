import java.util.*;

public class MathTest
{
	public static void main(String[] args)
	{
//		Random generator = new Random();
		Scanner scan = new Scanner(System.in);
		String firstName, lastName;
		
//		**!Note!**String is not a primitive data type/reserved word,
//		therefore eclipse and other IDE's will not syntax highlight this 
//		data type preceding variable declaration
		
//		System.out.print("\n\t Enter first name: ");
//		firstName = scan.next();
//		System.out.print("\n\t Enter last name: ");
//		lastName = scan.next();
		
//		int num = generator.nextInt((90)+10);
//		int randomNum = (int)(Math.random()* 90+10);
		int randomNum = (int)(Math.random()*25+65);
		char map = (char)(randomNum);
		
//		char firstNameLetter=firstName.charAt(0);
//		char lastNameLetter=lastName.charAt(0);
		
//		System.out.println(firstNameLetter + " " + lastNameLetter + " " + randomNum);
		System.out.println(map + " " + randomNum);
		
		scan.close();
	}
}