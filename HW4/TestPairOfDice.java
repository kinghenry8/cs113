import java.util.*;

public class TestPairOfDice 
{
	public static void main(String[] args)
	{
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter face value (integer): ");
		int firstRoll = scan.nextInt();
		System.out.println("Please enter color: ");
		String firstColor = scan.nextLine();
		System.out.println("Please enter second face value (integer): ");
		int secondRoll = scan.nextInt();
		System.out.println("Please enter second color: ");
		String secondColor = scan.nextLine();
		
		PairOfDice p1 = new PairOfDice();
		Die d1 = new Die(firstRoll,firstColor);
		Die d2 = new Die(secondRoll,secondColor);
		
		p1.setDie1(d1);
		p1.setDie2(d2);
		
		int diceProduct = d1.getFaceValue()*d2.getFaceValue();
		
		char firstChar1 = firstColor.charAt(0);
		char firstChar2 = secondColor.charAt(0);
		
		
		System.out.println(diceProduct+firstChar1+firstChar2);
		
		scan.close();
		
	}
}