
/*public class Die
{
	public int faceValue,colorChoice;
	public String color;
	
	public void roll()
	{
		faceValue =(int)(Math.random()*6)+1;
		colorChoice = (int)(Math.random()*1);
		
		if (colorChoice == 0)
			color = "red";
		else
			color = "blue";
	}
	
	public int getFaceValue()
	{
		return faceValue;
	}
	public String getColorChoice()
	{
		return color;
	}
}*/
public class Die
{
	private int faceValue;
	private String color;
		
	public Die()
	{
		faceValue = 1;
		color = "red";
	}
	
	public Die(int val, String color1)
	{
		faceValue = val;
		color = color1;
	}
	
	public void roll()
	{
		faceValue = (int)(Math.random()*6)+1;
	}
	
	public int getFaceValue()
	{
		return faceValue;
	}
	
	public String getColor()
	{
		return color;
	}
	
	public void setColor(String newColor)
	{
		color = newColor;
	}
	
	public void setFaceValue( int val )
	{
		faceValue = val;
	}
	
	public String toString()
	{
		String str = "Face Value = "+faceValue+", Color = "+color;
		return str;
	}
}
