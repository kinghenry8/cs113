

public class NiceDice
{
	
	public String comboDie(Die d1, Die d2)
	{
		int roll1, roll2,averageRoll;
		String color1,color2;
		
		roll1=d1.getFaceValue();
		roll2=d2.getFaceValue();
		
		color1=d1.getColor();
		color2=d2.getColor();
		
		averageRoll = (roll1+roll2)/2;
		String returnString = color1+"-"+color2+" "+"facevalue="+averageRoll;
		return returnString;
	}
	public static void main (String[] args)
	{
		NiceDice n1 = new NiceDice();
		Die d1 = new Die(5,"blue");
		Die d2 = new Die(3,"red");
		
		System.out.println(n1.comboDie(d1, d2));
	}
}