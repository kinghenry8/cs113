

public class Geometry 
{
	
	public double surface(int length,int width,int height)
	{
		int surfaceArea = (length*width*2)+(length*height*2)+(width*height*2);
		return surfaceArea;
		
	}
	
	public double rightTriangle(double sideA, double hypotenuseB)
	{
		double sideC=Math.sqrt(hypotenuseB*hypotenuseB-sideA*sideA);
		return (sideC);
	}
	
	public static void main(String[] args)
	{
		Geometry g1=new Geometry();
		double surface=g1.surface(4, 3, 5);
		
		System.out.println(surface);
		double side = g1.rightTriangle(4, 5);
		System.out.println(side);
		
	
				
	}
}