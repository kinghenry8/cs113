

public class PairOfDice
{

	private Die d1,d2;
	
	public PairOfDice() 
	{
		d1 = new Die(4,"red");
		d2= new Die(6,"blue");
		
	}

	
	public PairOfDice(Die d1,Die d2)
	{
		this.d1=d1;
		this.d2=d2;
	}
	
	public Die getDie1()
	{
		return d1;
	}
	
	public Die getDie2()
	{
		return d2;
	}
	
	public void setDie1(Die die)
	{
		this.d1=die;
	}
	public void setDie2(Die die)
	{
		this.d2=die;
	}
	
	public void rollDice()
	{
		this.d1.roll();
		this.d2.roll();
	}
	
	public String toString()
	{
		String retString = d1.getColor()+" "+d2.getColor();
		return retString;
	}
	public int pairSum()
	{
		int output = d1.getFaceValue()+d2.getFaceValue();
		return output;
	}
	
	public static void main (String[] args)
	{
		PairOfDice p1 = new PairOfDice();
		System.out.println(p1+" "+p1.pairSum());
		p1.rollDice();
		System.out.println(p1+" "+p1.pairSum());

	}
	
}