

public class Card
{
	private String suit;
	private int value;
	
	public Card()
	{
		suit = "spades";
		value = 1;
		
		
	}
	
	public Card(String suit, int value)
	{
		this.suit=suit;
		this.value=value;
	}
	

	public static void main (String[] args)
	{
		
	}
	
	public String getSuit()
	{
		return (this.suit);
	}
	
	public void setSuit(String suit)
	{
		this.suit= suit;
	}
	public int getValue()
	{
		return (this.value);
	}
	
	public void setValue(int value)
	{
		this.value= value;
	}
	
	public String toString ()
	{
		
		String retString = this.value + " of " + this.suit;
		return (retString);
	}
	
	public boolean equals(Card c1)
	{
		return(this.value==c1.getValue() && this.suit==c1.getSuit());
	}

	
}

