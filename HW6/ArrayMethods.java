import java.util.*;

public class ArrayMethods
{
	
	public Integer[] setup (Integer[] list)
	{
		ArrayList<Integer> setup = new ArrayList<Integer>();
		for (int i = 0; i < list.length; i++) {
			if (list[i]%2!=0)
				setup.add(-1);
			else
				setup.add(i);
			
		}
		Integer[] setUp = setup.toArray(new Integer[setup.size()]);
		return setUp;
	}
	public int[] fibo (int[] list)
	{
		int length = 3;
	    int[] series = new int[length];
	    series[0] = 0;
	    series[1] = 1;
	    for (int i = 2; i < length; i++) {
			series[i]= series[i-1]+series[i-2];
		}
    System.out.println(series);
	return series;
	}
	
	public ArrayList<Integer> pieces (ArrayList<Integer> array)
	{
		ArrayList<Integer> result = new ArrayList<Integer>();
		
		Iterator<Integer> itr = array.iterator();
		
		
		
		ArrayList<Integer> tmp = new ArrayList<Integer>();
		while(itr.hasNext()){
			tmp.add(itr.next());
			if (tmp.size()==5)
			{
				int sum = 0;
				for (int i = 0; i < tmp.size(); i++) {
					sum += tmp.get(i);
				}
				result.add(sum);
			}
				
		}
		return result;
		
	}
	
	public static void main(String[] args)
	{
		
	}
}