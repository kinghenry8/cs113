import java.util.*;

public class NumsandString
{

	
	public boolean isPerfect(int num)
	{
		ArrayList<Integer> perfects = new ArrayList<Integer>();
		
		for (int i = 0;i<num;i++)
			if (num%i==0)
				perfects.add(i);
			else
				continue;
		
		Integer num1 =0;
		Integer num2=0;
		for (int i=0; i<perfects.size();i++ )
			num1=perfects.get(i);
			num2= num1 +num2;
		return(num2==num);
				
		
	}
	
	public int allPerfect(int end)
	{
		NumsandString n1 = new NumsandString();
		int perfects = 0;
		for (int i = 0; i < end; i++) {
			if (n1.isPerfect(i))
				perfects=perfects+i;
		}
		return perfects;
	}
	
	public boolean isPalindrome(String str) {
		  int n = str.length();
		  for (int i=0;i<(n / 2) + 1;i++) {
		     if (str.charAt(i) != str.charAt(n - i - 1)) {
		         return false;
		     }
		  }

		  return true;
		}
	public static void main(String[] args)
	{
		NumsandString string = new NumsandString();
		System.out.println(string.isPalindrome("m"));
	}
}