import java.util.*;

public class Letter
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		String input;
		
		System.out.println("Please enter a two character string:");
		input = scan.next();
		
		char char1,char2,charAvg;
		int charVal1, charVal2,charValAvg;
		
		char1 = input.charAt(0);
		char2 = input.charAt(1);
		
		charVal1 = (int)(char1);
		charVal2 = (int)(char2);
		charValAvg = (charVal1+charVal2)/2;
				
		charAvg = (char)(charValAvg);
		
		System.out.print(charAvg);
		
		scan.close();
		
	}
}