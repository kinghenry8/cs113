import java.util.*;

public class Coins
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		int pennyVal=1, nickelVal=5, dimeVal=10, quarterVal=25;
		int numPennies, numNickels, numDimes, numQuarters,centVal;
		int remainder;
		
		System.out.println("Enter value in cents:");
		centVal = scan.nextInt();
		
		numQuarters = centVal/quarterVal;
		remainder = centVal%quarterVal;
		numDimes = remainder / dimeVal;
		remainder = remainder%dimeVal;
		numNickels = remainder/nickelVal;
		remainder = remainder%nickelVal;
		numPennies = remainder/pennyVal;
	
		System.out.print(numQuarters + " quarters, ");
		System.out.print(numDimes + " dimes, ");
		System.out.print(numNickels + " nickels, ");
		System.out.print(numPennies + " pennies.");
		
		scan.close();
		
	}
}
