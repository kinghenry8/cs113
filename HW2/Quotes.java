import java.util.*;

public class Quotes
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		String firstString, secondString;
		
		System.out.println("Please enter first string parameter:");
		firstString = scan.next();
		
		System.out.println("Please enter second string parameter:");
		secondString = scan.next();
		
		String concatString = (firstString + " " + secondString);
		
		System.out.println((char)34+concatString+(char)34);
		
		scan.close();
	}
}