public class CoinValues
{
	public static void main(String[] args)
	{
		int numQuarters,numDimes,numNickels,numPennies;
		double valQuarters,valDimes,valNickels,valPennies,totalValue;
		
		numQuarters=5;
		numDimes=10;
		numNickels=1;
		numPennies=25;
		
		valQuarters=.25;
		valDimes=.10;
		valNickels=.05;
		valPennies=.01;
		
		totalValue=(numQuarters*valQuarters)+(numDimes*valDimes)+
				(numNickels*valNickels)+(numPennies*valPennies);
		
		System.out.println(totalValue);
		
	}
}
