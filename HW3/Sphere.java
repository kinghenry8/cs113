

    public class Sphere

    { 
    	private double radius, diameter,surfaceArea,volume;
    	
    	

		public double getDiameter() {
			return diameter;
		}


		public void setVolume(double volume) {
			this.volume = volume;
		}


		public void setDiameter(double diameter) {
			this.diameter = diameter;
		}
		
    	public Sphere(Double diameter) 
    	{  

    		this.diameter = diameter;
    		radius = diameter/2;
    		
    		
    	} 
    	
    	public double getSurfaceArea()
    	{
    		surfaceArea = (4.0)*Math.PI*Math.pow(radius,2);
    		return surfaceArea;
    	}

		public double getVolume() {
			volume=(4.0 / 3) * Math.PI * Math.pow(radius, 3);
			return volume;
		}
    	
    	public static void main(String[] args)
    	{
    		Sphere sp1 = new Sphere(5.0);
    		System.out.println("Surface area = " + sp1.getSurfaceArea());
    		System.out.println("Volume = " + sp1.getVolume());
    	}
 
    }

    