
    public class Circle

    { 
    	private double radius;
		private double circum;
	
    	
    	

		public double getRadius() {
			return radius;
		}


		public void setCircum(double circum) {
			this.circum = circum;
		}


		public void setRadius(double radius) {
			this.radius = radius;
		}
		
    	public Circle(Double radius) 
    	{  

    		this.radius = radius;
    		
    		
    		
    	} 
    	
    	public double getCircum()
    	{
    		
    		double circum = Math.PI*radius*2;
    		return circum;
    	}

		public double getArea() {
			
			double area = Math.PI * Math.pow(radius, 2);
			return area;
		}
    	
    	public static void main(String[] args)
    	{
    		Circle circ1 = new Circle(5.0);
    		//double circumVal = circ1.getCircum();
    		//double areaVal = circ1.getArea();
    		//String circumPrint = String.valueOf(circumVal);
    		//String areaPrint = String.valueOf(areaVal);
    		System.out.println("Circumference = " + circ1.getCircum());
    		System.out.println("Area = " + circ1.getArea());
    	}
 
    }

    