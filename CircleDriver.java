import java.util.Scanner;


    public class CircleDriver

    { 
    	


    	public static void main(String[] args) 
    	{  
    		Scanner scan = new Scanner(System.in);
    		
    		System.out.println("enter the radius of circle # 1:");
    		double radius1 = scan.nextDouble();
    		Circle userC1 = new Circle(radius1);
    		
    		
    		System.out.println("enter the radius of circle # 2:");
    		double radius2 = scan.nextDouble();
    		Circle userC2 = new Circle(radius2);
    		
    		Double circumf1 = userC1.getCircum();
    		Double areaC2 = userC2.getArea();
    		
    		System.out.println("Circumference of circle 1 is: " + circumf1 );
    		System.out.println("Area of circle 2 is: "+areaC2);
    		scan.close();
    	} 

	
    
    }