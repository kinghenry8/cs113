import java.util.*;

public class Average
{
	private static Scanner scan;

	public static void main(String[] args)
	{
		scan = new Scanner (System.in);
		
		int num1,num2;
		
		System.out.print ("\n\t Enter first number = ");
		num1=scan.nextInt();
		
		System.out.print ("\n\t Enter second number = ");
		num2=scan.nextInt();
		
		
		double result = (double)(num1 + num2)/2.0;
		System.out.println(result);
	}
}